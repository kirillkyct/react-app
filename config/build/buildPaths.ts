import path from "path";

import {BuildPaths} from "./types/config";

export function buildPaths(): BuildPaths {
  return {
    entry: path.resolve(__dirname, '..', '..', 'src', 'index.tsx'),
    build: path.resolve(__dirname, '..', '..', 'dict'),
    html: path.resolve(__dirname, '..',  '..','public', 'index.html'),
  }
}