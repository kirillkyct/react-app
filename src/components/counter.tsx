import {useState} from "react";
import s from "./counter.module.scss";

export const Counter = () => {
  const [count, setCount] = useState(0);

  function incrementCount() {
    setCount((value) => value + 1);
  }

  return (
    <div>
      <button className={s.button} onClick={incrementCount}>{count}</button>
    </div>
  );
}