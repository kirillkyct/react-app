import {Configuration} from 'webpack';

import {buildWebpackConfig} from "./config/build/buildWebpackConfig";
import {buildPaths} from "./config/build/buildPaths";
import {BuildEnv} from "./config/build/types/config";

export default (env: BuildEnv): Configuration => {
  const mode = env.mode || 'development';
  const port = env.port || 3000;
  const isDev = mode === 'development';

  const config: Configuration = buildWebpackConfig({
    paths: buildPaths(),
    mode,
    isDev,
    port,
  });

  return config;
};